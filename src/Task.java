import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 * Tarea que el usuario tiene pendientr. Genera su propia casilla, texto y botón para eliminarse
 */
public class Task extends HBox{

    /**
     * Constructor de Task
     * @param task Texto que describe la tarea pendiente
     * @param parent Objeto que contiene todas las tareas
     * @param state true para crear la tarea como realizada (tachada).
     *              Esto es útil al regenerar la lista de tareas de una sesión
     *              anterior (todo.txt).
     */
    public Task(String task, ToDoList parent, Boolean state){
        setSpacing(10);
        CheckBox checkBox = new CheckBox();
        Text label = new Text(task);
        label.getStyleClass().add("task-label");
        label.wrappingWidthProperty().set(150);
        Button delete = new Button("X");
        delete.getStyleClass().remove("button");
        delete.setOnAction(e-> {
            parent.removeTask(this);
        });

        checkBox.setOnAction(e-> {
            if (checkBox.isSelected()){
                label.setStyle("-fx-strikethrough: true;-fx-opacity: 0.5;");
            } else {
                label.setStyle("-fx-strikethrough: false;-fx-opacity: 1;");
            }
        });
        checkBox.setSelected(state);
        if (state){
            label.setStyle("-fx-strikethrough: true;-fx-opacity: 0.5;");
        }
        getChildren().addAll(checkBox, label, delete);
    }
}
