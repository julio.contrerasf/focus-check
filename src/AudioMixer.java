import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/** AudioMixer contiene la vista de todos los SimpleAudioMixer, y los posiciona en 2 columnas
 */
public class AudioMixer extends Widget {
    //Group root = new Group();
    //Scene scene = new Scene(root, 300, 250);
    private static final String rSound = AudioMixer.class.getResource("audio/lluvia.mp3").toExternalForm();
    private static final String fSound = AudioMixer.class.getResource("audio/fire.mp3").toExternalForm();
    private static final String kSound = AudioMixer.class.getResource("audio/keyboard.mp3").toExternalForm();
    private static final String tSound = AudioMixer.class.getResource("audio/traffic.mp3").toExternalForm();
    private static final String sSound = AudioMixer.class.getResource("audio/sea.mp3").toExternalForm();
    private static final String foSound = AudioMixer.class.getResource("audio/forest.mp3").toExternalForm();
    private static final String bSound = AudioMixer.class.getResource("audio/birds.mp3").toExternalForm();
    private static final String wSound = AudioMixer.class.getResource("audio/writing.mp3").toExternalForm();

    private String rIcon = "icons/rainy.png";
    private String fIcon = "icons/fire.png";
    private String kIcon = "icons/keyboard.png";
    private String tIcon = "icons/car.png";
    private String sIcon = "icons/sea.png";
    private String foIcon = "icons/tree.png";
    private String bIcon = "icons/bird.png";
    private String wIcon = "icons/note.png";

    private HBox view;

    /**
     * Constructor de AudioMixer
     * @param icono Ubicación de la imagen que actúa como ícono para abrir y cerrar el AudioMixer
     */
    public AudioMixer(String icono) {
        super(icono);
        view = new HBox();
        super.createAnimation(view);

        SimpleAudioMixer rainAudio = new SimpleAudioMixer(rSound, rIcon,0.6);
        SimpleAudioMixer fireAudio = new SimpleAudioMixer(fSound, fIcon,1);
        SimpleAudioMixer keyboardAudio = new SimpleAudioMixer(kSound, kIcon,0.7);
        SimpleAudioMixer trafficAudio =new SimpleAudioMixer(tSound, tIcon,1);
        SimpleAudioMixer seaAudio= new SimpleAudioMixer(sSound,sIcon, 1);
        SimpleAudioMixer forestAudio= new SimpleAudioMixer(foSound,foIcon, 1.5);
        SimpleAudioMixer birdAudio= new SimpleAudioMixer(bSound,bIcon, 0.8);
        SimpleAudioMixer writingAudio= new SimpleAudioMixer(wSound,wIcon, 1.2);

        VBox col1 = new VBox(20);
        VBox col2 = new VBox(20);
        col1.getChildren().addAll(rainAudio,fireAudio,keyboardAudio,trafficAudio);
        col2.getChildren().addAll(seaAudio,birdAudio,forestAudio,writingAudio);
        view.getChildren().addAll(col1,col2);
        view.getStyleClass().add("mixer");
    }

}
