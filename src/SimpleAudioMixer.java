import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Slider;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/** SimpleAudioMixer le da funcionalidad a cada slider con su respectivo icono, este slider
 * aumenta o disminuye el volumen del sonido elegido, el cual se repite en bucle.
 */
public class SimpleAudioMixer extends Group{
    private ImageState iconState;
    private double audioVolume;
    private int flag;

    /**
     * Constructor de SimpleAudioMixer
     * @param audio Ubicación del audio a controlar
     * @param icon Ubicación de la imagen para activar/desactivar el sonido
     * @param scale Nivel base para el volumen. Usar valores en torno a 1 (uno)
     */
    public SimpleAudioMixer(String audio, String icon, double scale)
    {
        //Slider del sonido
        Slider audioSlider = new Slider(0, 100, 0);

        //icono del sonido
        Image imageIcon = new Image(icon);
        //Vista y ajuste de tamaño del icono
        ImageView iconView = new ImageView();
        iconView.setImage(imageIcon);
        iconView.setFitHeight(30);
        iconView.setFitWidth(30);

        //ajusta el color inicial del icono
        ColorAdjust colorAdjust = new ColorAdjust();
        iconState = ImageState.BLACK;
        colorAdjust.setSaturation(-70);
        iconView.setOpacity(0.6);
        iconView.setEffect(colorAdjust);

        //crea los sonidos
        Media audioSound = new Media(audio);
        MediaPlayer audioPlayer = new MediaPlayer(audioSound);

        //El volumen comienza en 20 una vez se encienda
        audioSlider.setValue(20);
        flag=0;

        //Loop
        audioPlayer.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                audioPlayer.seek(Duration.ZERO);
                audioPlayer.play();
            }
        });

        //Cambia color del icono al presionar, detiene/reproduce el sonido
        iconView.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            iconState = iconState== ImageState.COLOR ? ImageState.BLACK : ImageState.COLOR;
            if (iconState == ImageState.BLACK) {
                colorAdjust.setSaturation(-40);
                iconView.setOpacity(0.6);
                audioPlayer.stop();
            } else if (iconState == ImageState.COLOR) {
                flag=1;
                colorAdjust.setSaturation(0);
                iconView.setOpacity(1);
                //Volumen se inicia en 20, no olvidar el audio escalado
                if (flag==1){
                    audioVolume=0.2*scale;
                    audioPlayer.setVolume(audioVolume);
                }
                audioPlayer.play();
            }
            iconView.setEffect(colorAdjust);
            event.consume();
        });




        //Regula el Volumen
        audioSlider.valueProperty().addListener(
                new ChangeListener<Number>() {
                    public void changed(ObservableValue<? extends Number >
                                                observable, Number oldValue, Number newValue)
                    {
                        audioVolume=(double) newValue *scale/100;
                        audioPlayer.setVolume(audioVolume);

                        //Al usar el slider quiero que se reprod0uzca sonido igual
                        iconState=ImageState.COLOR;
                        colorAdjust.setSaturation(0);
                        iconView.setOpacity(1);
                        iconView.setEffect(colorAdjust);
                        audioPlayer.play();

                        if (audioVolume==0.0){
                            iconState=ImageState.BLACK;
                            colorAdjust.setSaturation(-40);
                            iconView.setOpacity(0.6);
                            iconView.setEffect(colorAdjust);
                            audioPlayer.stop();
                        }

                    }


                });


        HBox rainBox = new HBox(10); //Cajita para poner los botones
        rainBox.setAlignment(Pos.CENTER);
        rainBox.getChildren().addAll(iconView, audioSlider);


        getChildren().add(rainBox);

    }
}