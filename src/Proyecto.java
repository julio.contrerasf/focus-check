import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.control.Label;


import java.util.Optional;

/**
 * Clase principal
 */
public class Proyecto extends Application{

    /**
     * Genera la interfaz gráfica
     * @param primaryStage Ventana principal
     */
    public void start(Stage primaryStage) {
        BorderPane pane = new BorderPane();
        BackgroundCarousel bgCarousel = new BackgroundCarousel(pane);
        StackPane stackPane = new StackPane();
        setMenuPrincipal(pane, stackPane);

        pane.setCenter(stackPane);

        Scene scene = new Scene(pane, 1080, 608);
        scene.getStylesheets().add(getClass().getResource("estilo.css").toExternalForm());
        scene.addEventFilter(KeyEvent.KEY_PRESSED, e -> bgCarousel.changeBackground(e));
        scene.addEventFilter(KeyEvent.KEY_PRESSED, e -> todo.addTaskEnter(e));
        primaryStage.setOnHiding( event -> {closeWindowEvent(event);} );
        primaryStage.setTitle("Focus Check");
        primaryStage.getIcons().add(new Image("icons/icono.png"));
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Crea y configura el menú de widgets y el StackPane donde se abren/cierran
     * @param pane BorderPane base, desde donde se ubican todas las vistas
     * @param stackPane StackPane que va en el centro de pane. Donde se abren/cierran los widgets
     */
    private void setMenuPrincipal(BorderPane pane, StackPane stackPane){
        VBox menuBox = new VBox(0);
        menuBox.setSpacing(30);
        menuBox.setPadding(new Insets(20));

        PomodoroTimer timer = new PomodoroTimer("icons/stopwatch.png");
        AudioMixer mixer = new AudioMixer("icons/mixericon.png");
        todo = new ToDoList("icons/checklist.png");

        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            todo.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event2 -> {
                event2.consume();
                return;
            });
            mixer.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event2 -> {
                event2.consume();
                return;
            });
            timer.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, event2 -> {
                event2.consume();
                return;
            });
            timer.close();
            mixer.close();
            todo.close();
        });

        stackPane.getChildren().addAll(timer.getNode(), mixer.getNode(), todo.getNode());

        menuBox.getChildren().addAll(timer.opener(), mixer.opener(), todo.opener());
        pane.setLeft(menuBox);

        Label l = new Label();
        l.setMinWidth(90);
        pane.setRight(l);
    }

    /**
     * Genera una alerta al cerrar la ventana principal para guardar o no la lista de tareas pendientes
     * @param event Evento de cierre de la ventana principal
     */
    private void closeWindowEvent(WindowEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.getButtonTypes().remove(ButtonType.OK);
        alert.getButtonTypes().remove(ButtonType.CANCEL);
        alert.getButtonTypes().add(ButtonType.NO);
        alert.getButtonTypes().add(ButtonType.YES);
        alert.setTitle("Cerrar Focus Check");
        alert.setContentText(String.format("\u00bfQuieres guardar la lista de tareas?"));
        Optional<ButtonType> res = alert.showAndWait();

        if(res.isPresent()) {
            if(res.get().equals(ButtonType.YES))
                todo.saveTasksToFile(true);
            else
                todo.saveTasksToFile(false);
        }
    }

    /**
     * Método main
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    private ToDoList todo;

}
