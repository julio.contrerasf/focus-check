import javafx.animation.ScaleTransition;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

/**
 * Clase padre de los widgets: objetos que tienen una vista que se abre/cierra con un ícono
 */
public class Widget {
    ImageView icon;
    protected Node nodo;
    private ScaleTransition widgetOpen;
    private ScaleTransition widgetClose;

    /**
     * Constructor de Widget
     * @param iconImg ubicación del archivo del ícono que actúa
     *               como botón toggle para abrir o cerrar la vista
     */
    public Widget(String iconImg){
        icon = new ImageView(new Image(iconImg));
        icon.setPickOnBounds(true);
    }

    /**
     * Las clases hijas deben llamar a este método luego del constructor super
     * @param view Vista de la clase hija que a la cual se le aplica la animación de cierre/apertura
     */
    protected void createAnimation(Node view){
        nodo = view;

        nodo.setScaleX(0);
        nodo.setScaleY(0);

        widgetOpen = new ScaleTransition(Duration.millis(300), nodo);
        widgetOpen.setFromX(0);
        widgetOpen.setFromY(0);
        widgetOpen.setToX(1);
        widgetOpen.setToY(1);

        widgetClose = new ScaleTransition(Duration.millis(300), nodo);
        widgetClose.setFromX(1);
        widgetClose.setFromY(1);
        widgetClose.setToX(0);
        widgetClose.setToY(0);

        icon.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if(nodo.getScaleX()==0){
                widgetOpen.play();
            }
            else{
                widgetClose.play();
            }
        });
    }

    /**
     * Ejecuta la animación de cierre
     */
    public void close(){
        if(nodo.getScaleX() != 0){
            widgetClose.play();
        }
    }

    /**
     * Entrega el ícono para posicionarlo en la escena principal
     * @return ícono que actúa de botón toggle para abrir o cerrar la vista
     */
    public ImageView opener(){
        return icon;
    }

    /**
     * Entrega la vista del widget (que se abre o cierra con el ícono) para
     * posicionarla en la escena principal
     * @return Vista del widget
     */
    public Node getNode(){
        return nodo;
    }
}
