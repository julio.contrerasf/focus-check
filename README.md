# Focus Check

![logo Focus Check](src/icons/icono.png)

Proyecto final para el curso ELO329 - Diseño y programación orientados a objetos, semestre 2022-1.

# Compilación y ejecución

Si se modifica la ruta de JavaFX directamente en el archivo `makefile`,
compilar con:
```
make
```
y ejecutar con:
```
make run
```
En caso contrario,
compilar con:
```
make JavaFX="/path/to/javafx/lib"
```
y ejecutar con:
```
make JavaFX="/path/to/javafx/lib" run
```

Para remover los archivos de compilación generados (.class) usar:
```
make clean
```

